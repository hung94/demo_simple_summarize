# -*- coding: utf8 -*-
"""
  Author:  Hung Bui
  Purpose: 
  Project: ML
  Created: 2/28/18
"""

from nltk.corpus import brown, stopwords, reuters
from nltk.cluster.util import cosine_distance
from operator import itemgetter
import numpy as np


def sentence_similarity(sent1, sent2, stopwords=None):  # @TODO need to improve, using word-embedding instead BoW
    if stopwords is None:
        stopwords = []

    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    all_words = list(set(sent1 + sent2))

    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)

    for w in sent1:
        if w in stopwords:
            continue
        vector1[all_words.index(w)] += 1

    for w in sent2:
        if w in stopwords:
            continue
        vector2[all_words.index(w)] += 1

    return 1 - cosine_distance(vector1, vector2)


# print 'Sentence similarity : ', sentence_similarity("I want to buy some food".split(),
#                                                     "I want to go to the market".split(),
#                                                     stopwords.words('english'))


# sentences = brown.sents('ca01')
sentences = reuters.sents('test/14826')
stop_words = stopwords.words('english')
print reuters.raw('test/14826')


def build_similarity_matrix(sentences, stopwords=None):
    S = np.zeros((len(sentences), len(sentences)))
    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):
            if idx1 == idx2:
                continue
            S[idx1][idx2] = sentence_similarity(sentences[idx1], sentences[idx2], stopwords)

    for idx in range(len(sentences)):
        S[idx] /= S[idx].sum()
    return S


def pagerank(A, eps=1e-4, d=0.85):
    P = np.ones(len(A)) / len(A)
    while True:
        new_P = np.ones(len(A)) * (1 - d) / len(A) + d * A.T.dot(P)
        delta = abs((new_P - P).sum())
        if delta <= eps:
            return new_P
        P = new_P


def textrank(sentences, top_n=5, stopwords=None):
    S = build_similarity_matrix(sentences, stopwords)
    sentence_ranks = pagerank(S)
    ranked_sentence_index = [item[0] for item in sorted(enumerate(sentence_ranks), key=lambda item: -item[1])]
    selected_sentences = sorted(ranked_sentence_index[:top_n])
    summary = itemgetter(*selected_sentences)(sentences)
    return summary


print 'Simple summarize for document :'
for idx, sentence in enumerate(textrank(sentences, stopwords=stop_words)):
    print "{0}. {1}".format(idx + 1, ' '.join(sentence))

